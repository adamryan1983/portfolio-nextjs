export const portfolioData = [
  {
  id: 1,
  blurb: '<p>I built this app using a tutorial offered by <a href="https://github.com/adrianhajdin" target="_blank">thejavascriptmastery</a></p><p>It involves the display of information relating to covid 19 cases both globally and by country. It was created using React, MaterialUI, html5, and CSS.</ p>',
  portURL:"https://github.com/adamryan1983/covid-19-tracker",
  portTitle:"Covid-19 Tracker App",
  tags:["#react","#javascript","#html","#materialUI"],
  image:"/images/project-covid.jpg"
  },

  {
    id: 2,
    blurb: '<p>I built this app using a tutorial offered by <a href="#" target="_blank">@DevEd</a></p><p>It allows you to show a simple task list, with options to show all, completed, or uncompleted. It was created using javascript, html5, and CSS.</p>',
    portURL: "https://github.com/adamryan1983/todo-List",
    portTitle: "To-Do App",
    tags: ["#javascript","#html/css"],
    image: "/images/todo-app.jpg"
  },

  {
    id: 3,
    blurb: '<p>I built this app using a tutorial offered by <a href="https://github.com/adrianhajdin/" target="_blank">thejavascriptmastery</a></p><p>This is a progressive web app which was designed to learn usage of APIs and PWA. It was created using React, javascript, html5, and CSS.</p>',
    portURL: "https://github.com/adamryan1983/PWA-Weather-App",
    portTitle: "Progressive Web App: Weather App",
    tags: ["#javascript","#html/css","react"],
    image: "/images/pwa-weather-app.webp"
  },

  {
    id: 4,
    blurb: '<p>I built this app during my first year of Programming studies at college</p><p>This is still a work in progress and still somewhat \'unfinished\'. A complete game can be finished but there is no AI incorporated beyond random choices for the computer. It was created using javascript, html5, and CSS.</p>',
    portURL: "https://github.com/adamryan1983/Go-Fish-Game",
    portTitle: "Go Fish game",
    tags: ["#javascript","#html/css"],
    image: "/images/go-fish.jpg"
  },

  {
    id: 5,
    blurb: '<p>I built this app as a tutorial with our instructor during Programming studies at college.</p><p>It\'s a simple web version of the classic game and incorporates some basic AI for the computer moves. It was created using javascript, html5, and CSS.</p>',
    portURL: "https://github.com/adamryan1983/js-connectfour",
    portTitle: "Connect Four game",
    tags: ["#javascript","#html/css"],
    image: "/images/connect-four.jpg"
  },

  {
    id: 6,
    blurb: '<p>I built this website from scratch for my side business, Adam Ryan Photography</p><p>It was built with the intentions of drafting a new website, as well as grasp the usage of CSS frameworks. It was created using Tailwind (CSS framework), javascript, html5, and CSS.</p>',
    portURL: "https://adamryanphotography.ca",
    portTitle: "Adam Ryan Photography website",
    tags: ["#javascript","#html/css", "tailwind css"],
    image: "/images/arp-page.jpg"
  }
  ]